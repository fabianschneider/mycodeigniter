<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * More Information and documentation on http://http://www.grocerycrud.com/ 
 */
class Crud extends CI_Controller {

    function __construct() {
        parent::__construct();

        /* Standard Libraries */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');
    }

    function output($output = null) {
        $this->load->view('crud.php', $output);
    }

    function car() {
        try {
            /* This is only for the autocompletion */
            $crud = new grocery_CRUD();

            $crud->set_theme('datatables');
            $crud->set_table('Car');
            $crud->set_subject('Car');
            $crud->required_fields('name');

            $output = $crud->render();

            $this->output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    function owner() {
        try {
            /* This is only for the autocompletion */
            $crud = new grocery_CRUD();

            $crud->set_theme('datatables');
            $crud->set_table('Owner');
            $crud->set_subject('Owner');
            $crud->required_fields('name');

            $output = $crud->render();

            $this->output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

}