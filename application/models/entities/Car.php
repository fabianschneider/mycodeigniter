<?php

namespace Entities;

/**
 * This is an example class, to show how a simple implementation
 * Doctrine works.
 *
 * @author fabianschneider
 */

/**
 * @Entity
 * @Table(name="Car") 
 */
class Car{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string") 
     */
    private $name;

    /**
     * @ManyToOne(targetEntity="Entities\Owner", inversedBy="cars")
     */
    private $owner;

    // ========================================================================
    // ************************************************************************
    // ========================================================================

    public function __construct() {
        // @Todo implement constructor
    }

    // ========================================================================
    // ************************** Getters & Setters ***************************
    // ========================================================================

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->title;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getOwner() {
        $this->owner;
    }


}

?>
